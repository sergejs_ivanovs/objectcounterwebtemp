<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.8.1
 * Time: 13:26
 */

namespace SRC\WorkingAreas\Controller;

use Silex\Application;
use SRC\Sources\Services\SourcesService;
use SRC\WorkingAreas\Services\WorkingAreaService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WorkingAreasController
{
    private $app;
    private $workingAreaService;
    private $sourcesService;

    public function __construct(Application $app,
                                WorkingAreaService $workingAreaService,
                                SourcesService $sourcesService
    )
    {
        $this->app = $app;
        $this->workingAreaService = $workingAreaService;
        $this->sourcesService = $sourcesService;
    }

    public function indexAction(Request $request)
    {
        $source_id = $request->query->get('source_id');

        $areas = $this->workingAreaService->getAll($source_id);
        $sources = $this->sourcesService->getAll();
        return $this->app['twig']->render('WorkingAreas/view/index.twig', [
            'areas' => $areas,
            'sources' => $sources
        ]);
    }

    public function openAreaAction($id)
    {
        $area = $this->workingAreaService->getById($id);
        $source = $this->sourcesService->getById($area['source_id']);
        $sources = $this->sourcesService->getAll();
        if (!$area) {
            return $this->app->redirect($this->app['url_generator']->generate('workingAreas'));
        }

        return $this->app['twig']->render('WorkingAreas/view/open.twig', [
            'area' => $area,
            'source' => $source,
            'sources' => $sources
        ]);
    }

    public function getAction($id)
    {
        $area = $this->workingAreaService->getById($id);
        if(!$area) {
            $response = ['status' => ERROR_CODE, 'message' => 'not found'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }

        $response = ['status' => SUCCESS_CODE, 'message' => 'Fetched', 'data' => $area];
        return $this->app->json($response, Response::HTTP_OK);
    }

    public function saveAction(Request $request)
    {
        $params = [
            'id' => $request->get('id'),
            'sourceId' => $request->get('sourceId'),
            'areaName' => $request->get('areaName'),
            'areaDescription' => $request->get('areaDescription'),
        ];
        $error = [];
        if (!$params['areaName']) {
            $error[] = [
                'field' => 'srcName',
                'message' => 'Working area name required'
            ];
        }
        if (!empty($error)) {
            $response = ['status' => ERROR_CODE, 'message' => $error[0]['message'], 'error' => $error];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            $areaId = $this->workingAreaService->saveArea($params);
        } catch (\Exception $e) {
            $response = ['status' => ERROR_CODE, 'message' => 'Error on save service'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }

        $areaUrl = $this->app['url_generator']->generate('workingAreaOpen', ['id' => $areaId]);

        $response = ['status' => SUCCESS_CODE, 'message' => 'Saved', 'data' => ['redirectUrl' => $areaUrl]];
        return $this->app->json($response, Response::HTTP_OK);
    }

    public function deleteAction($id)
    {
        if (!$id) {
            $response = ['status' => ERROR_CODE, 'message' => 'Invalid arguments'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }
        try {
            $this->workingAreaService->removeById($id);
        } catch (\Exception $e) {
            $response = ['status' => ERROR_CODE, 'message' => $e->getMessage()];
            return $this->app->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $response = ['status' => SUCCESS_CODE, 'message' => 'removed'];
        return $this->app->json($response, Response::HTTP_OK);
    }
}