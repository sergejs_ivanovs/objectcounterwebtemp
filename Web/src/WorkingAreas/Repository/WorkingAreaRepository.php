<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.10.1
 * Time: 14:32
 */

namespace SRC\WorkingAreas\Repository;

use Doctrine\DBAL\Connection;
use PDO;
use SRC\Sources\Repository\SourcesRepository;

class WorkingAreaRepository
{
    private $connection;
    const TABLE = 'working_area';

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAll()
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from(self::TABLE, 'area');
        $results = $queryBuilder->execute()->fetchAll();
        return $results;
    }

    public function getBySourceId($sourceId)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from(self::TABLE, 'area')
            ->where("area.source_id = :source_id")
            ->setParameter(':source_id', $sourceId);
        $results = $queryBuilder->execute()->fetchAll();
        return $results;
    }

    public function getById($id)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from(self::TABLE, 'area')
            ->where("area.id = :id")
            ->setParameter(':id', $id);
        $results = $queryBuilder->execute()->fetch();
        return $results;
    }

    public function save(Array $params)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $saved = $queryBuilder
            ->insert(self::TABLE)
            ->values([
                'name' => ':name',
                'source_id' => ':srcId',
                'client_id' => ':client_id',
                'description' => ':description',
            ])
            ->setParameter(':name', $params['areaName'])
            ->setParameter(':srcId', $params['sourceId'])
            ->setParameter(':client_id', $params['clientId'])
            ->setParameter(':description', $params['areaDescription'])
            ->execute();
        if ($saved) {
            $id = $this->connection->lastInsertId();
            return $id;
        } else {
            throw new \Exception('Not saved');
        }
    }

    public function update(Array $params)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->update(self::TABLE, 'area')
            ->set('name', ':name')
            ->set('source_id', ':srcId')
            ->set('client_id', ':client_id')
            ->set('description', ':description')
            ->where('area.id = :id')
            ->setParameter(':id', $params['id'])
            ->setParameter(':name', $params['areaName'])
            ->setParameter(':srcId', $params['sourceId'])
            ->setParameter(':client_id', $params['clientId'])
            ->setParameter(':description', $params['areaDescription'])
            ->execute();
    }

    public function removeById($id)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder->delete(self::TABLE)
            ->where('id = :id')
            ->setParameter(':id', $id);
        return $queryBuilder->execute();
    }


}