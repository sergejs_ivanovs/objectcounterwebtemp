$(function() {
    $('.sources-add-area-btn').click(function() {
        var sourceid = $(this).data('sourceid');
        console.log(sourceid);
        if(typeof sourceid == 'undefined' && $.getUrlhas("source_id")) {
            sourceid = $.getUrlVar("source_id");
        }
        console.log(sourceid);
        $('#area-name').val('');
        $('#area-description').val('');
        $('#area-source-id').val(sourceid);
        $('.modal-overlay').addClass('hidden');
    });
    $('.area-edit-btn').click(function() {
        var id = $(this).data('id');
        $('.modal-overlay').removeClass('hidden');
        $.ajax({
            url: '/api/v1/working_area/get/'+id,
            method: "GET",
            success: function(result) {
                $('.modal-overlay').addClass('hidden');
                if(result.status == 'success') {
                    showSuccess(result.message);
                    $('#area-id').val(result.data.id);
                    $('#area-name').val(result.data.name);
                    $('#area-description').val(result.data.description);
                    $('#area-source-id').val(result.data.source_id);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                checkXhr(jqXHR);
                $('.modal-overlay').addClass('hidden');
                showError(jqXHR.responseJSON.message);
                console.log(jqXHR.responseJSON);
            }
        });
    });
    $('#modal-source-create-area-btn').click(function() {
        var id = $('#area-id').val();
        var sourceId = $('#area-source-id').val();
        var areaName = $('#area-name').val();
        var areaDescription = $('#area-description').val();
        $('.modal-overlay').removeClass('hidden');
        $.ajax({
            url: '/api/v1/working_area/save',
            method: "POST",
            dataType: "JSON",
            data: {
                id: id,
                sourceId: sourceId,
                areaName: areaName,
                areaDescription: areaDescription
            },
            success: function(result) {
                $('.modal-overlay').addClass('hidden');
                if(result.status == 'success') {
                    showSuccess(result.message);
                    if(result.data.redirectUrl) {
                        location.replace(result.data.redirectUrl);
                    } else {
                        location.reload();
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                checkXhr(jqXHR);
                $('.modal-overlay').addClass('hidden');
                showError(jqXHR.responseJSON.message);
                console.log(jqXHR.responseJSON);
            }
        });
    });
});