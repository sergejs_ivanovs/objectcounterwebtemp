/**
 * Created by vadims on 18.11.1.
 */
var polygons = {};
var currentPolygon = null;
var draw = null;
var video = null;
var limit = 2;

$(function () {
    setTimeout(function() {prepareSvgArea()}, 100);

    $('.add_polygon_button').click(function() {
        if(!draw) {
            alert('Drawing area not prepared');
            return false;
        }
        if(Object.keys(polygons).length >= limit) {
            alert('Max polygon count ' + limit);
            return false;
        }
        disableDrawingButtons();
        var method = $(this).data('figure');
        var poly = null;
        if(method == "polygon") {
            poly = draw.polygon().draw();
        } else if(method == "ellipse") {
            poly = draw.ellipse().draw();
        }
        poly.polygon_type = method;
        poly.attr({
            fill: 'none'
            , 'fill-opacity': 0.5
            , stroke: '#111'
            , 'stroke-width': 8
        });

        poly.on('drawstart', function(e){
            document.addEventListener('keydown', function(e){
                if(e.keyCode == 13){
                    poly.draw('done');
                    poly.off('drawstart');
                }
            });
        });

        poly.on('drawstop', function(){
            console.log('drawstop');
            enableDrawingButtons();
            //convert ellipse to polygon
            if(poly.polygon_type == "ellipse") {
                var path = poly.toPath();
                var toPoly = path.toPoly();
                poly.remove();
                path.remove();
                poly = toPoly;
                poly.polygon_type = 'ellipse';
            }
            poly = addPolyEvents(poly);

            poly.polygon_name = (new Date()).getTime()+'';
            if(!poly.polygon_id) {
                poly.polygon_id =  poly.polygon_name;
            }
            polygons[poly.polygon_id] = poly;
            saveTriggerArea(poly);

            console.log(poly);

        });
        poly.on('drawdone', function(){
            console.log('drawdone');
        });
    });
    $('.remove_polygon_button').click(function() {
        console.log(currentPolygon, polygons);
        if(currentPolygon) {
            removeTriggerArea(currentPolygon);
        }
    });
});

function prepareSvgArea() {
    video = document.getElementById("video");
    if(!video || video.videoWidth < 1 || video.videoHeight< 1) {
        setTimeout(function() {prepareSvgArea()}, 1000);
    } else {
        console.log(video.videoWidth, video.videoHeight);
        draw = SVG('drawing').size(video.videoWidth, video.videoHeight);
        fetchTriggers();
    }
}

function deselectAll() {
    $.each(polygons, function(index, poly) {
        poly.draggable(false).resize('stop').selectize(false).selectize(false, {deepSelect:true})
    });
}

function setSelectPointsStyle() {
    $(document).find('.svg_select_points').attr('stroke', '#000').attr('fill', '#fff');
}
function disableDrawingButtons() {
    $('#polygon-btn').attr('disabled', true);
    $('#ellipse-btn').attr('disabled', true);
}
function enableDrawingButtons() {
    $('#polygon-btn').attr('disabled', false);
    $('#ellipse-btn').attr('disabled', false);
}
function saveTriggerArea(poly) {
    var workingAreaId = $('#working-area-id').val();
    var id = poly.polygon_id;
    var name = poly.polygon_name;
    var type = poly.polygon_type;
    var polygon = poly.array().value;
    $.ajax({
        url: '/api/v1/trigger_areas/save',
        method: "POST",
        dataType: "JSON",
        data: {
            id: id,
            name: name,
            type: type,
            workingAreaId: workingAreaId,
            polygon: polygon,
            method_id: null,
            algorithm: null
        },
        success: function(result) {
            $('.modal-overlay').addClass('hidden');
            if(result.status == 'success') {
                showSuccess(result.message);
            } else {

            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            checkXhr(jqXHR);
            $('.modal-overlay').addClass('hidden');
            showError(jqXHR.responseJSON.message);
            console.log(jqXHR.responseJSON);
        }
    });
}
function removeTriggerArea(removeCandidate) {
    $.ajax({
        url: '/api/v1/sources/delete/'+removeCandidate.polygon_id,
        method: "DELETE",
        success: function(result) {
            console.log('fetchTriggers', result);
            $('.modal-overlay').addClass('hidden');
            if(result.status == 'success') {
                showSuccess(result.message);
                delete polygons[removeCandidate.polygon_id];
                removeCandidate.draggable(false).resize('stop').selectize(false).selectize(false, {deepSelect:true});
                removeCandidate.remove();
            } else {
                showError(result.message);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            checkXhr(jqXHR);
            $('.modal-overlay').addClass('hidden');
            showError(jqXHR.responseJSON.message);
            console.log(jqXHR.responseJSON);
        }
    });
}
function fetchTriggers() {
    var workingAreaId = $('#working-area-id').val();
    $.ajax({
        url: '/api/v1/trigger_areas/get/'+workingAreaId,
        method: "GET",
        success: function(result) {
            console.log('fetchTriggers', result);
            $('.modal-overlay').addClass('hidden');
            if(result.status == 'success') {
                showSuccess(result.message);
                plotPoygons(result.data);
                enableDrawingButtons();
            } else {
                showError(result.message);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            checkXhr(jqXHR);
            $('.modal-overlay').addClass('hidden');
            showError(jqXHR.responseJSON.message);
            console.log(jqXHR.responseJSON);
        }
    });
}
function plotPoygons(data) {
    if(!data || typeof data == 'undefined') {
        return false;
    }
    data.forEach(function(area, index) {
        try {
            var poly = draw.polygon(area.polygon);
            poly.attr({
                fill: 'none'
                , 'fill-opacity': 0.5
                , stroke: '#111'
                , 'stroke-width': 8
            });
            poly.polygon_id = area.id;
            poly.polygon_name = area.name;
            poly.polygon_type = area.polygon_type;
            poly = addPolyEvents(poly);
            polygons[poly.polygon_id] = poly;
        } catch (e) {
            console.log(e);
        }
    });
}
function addPolyEvents(poly) {
    if(poly.polygon_type == "ellipse") {
        poly.on('click', function() {
            deselectAll();
            this.selectize().resize().draggable();
            setSelectPointsStyle();
            currentPolygon = poly;
            setCurrentPolygonInfo(poly);
        });
        poly.polygon_type = 'ellipse';
    } else {
        poly.on('click', function() {
            deselectAll();
            this.selectize().selectize({deepSelect:true}).resize().draggable();
            setSelectPointsStyle();
            currentPolygon = poly;
            setCurrentPolygonInfo(poly);
        });
        poly.polygon_type = 'polygon';
    }
    poly.on('dragend', function() {
        saveTriggerArea(this);
        console.log('dragend', this.node.id, this);
    });
    poly.on('resizedone', function() {
        saveTriggerArea(this);
        console.log('resizedone', this.node.id, this);
    });
    return poly;
}
function setCurrentPolygonInfo(poly) {
    $('#trigger-id').text(poly.polygon_id);
    $('#trigger-name').text(poly.polygon_name);
}