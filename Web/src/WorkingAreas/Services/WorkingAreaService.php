<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.10.1
 * Time: 14:31
 */

namespace SRC\WorkingAreas\Services;

use SRC\WorkingAreas\Repository\WorkingAreaRepository;

class WorkingAreaService
{
    private $workingAreaRepository;

    public function __construct(WorkingAreaRepository $workingAreaRepository)
    {
        $this->workingAreaRepository = $workingAreaRepository;
    }

    public function getAll($sourceId = null)
    {
        if (!$sourceId || $sourceId == 'all') {
            return $this->workingAreaRepository->getAll();
        } else {
            return $this->workingAreaRepository->getBySourceId($sourceId);
        }
    }

    public function saveArea(Array $params)
    {
        $id = $params['id'];

        $params['clientId'] = '0';
        if ($id && $this->workingAreaRepository->getById($id)) {
            $this->workingAreaRepository->update($params);
        } else {
            $id = $this->workingAreaRepository->save($params);
        }

        return $id;
    }

    public function getById($id)
    {
        return $this->workingAreaRepository->getById($id);
    }

    public function removeById($id)
    {
        if (!$this->workingAreaRepository->getById($id)) {
            throw new \Exception('not found');
        }
        return  $this->workingAreaRepository->removeById($id);
    }
}