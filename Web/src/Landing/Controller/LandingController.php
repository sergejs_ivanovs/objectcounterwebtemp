<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 16.18.10
 * Time: 10:55
 */

namespace SRC\Landing\Controller;

use Silex\Application;

class LandingController
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function indexAction()
    {
        return $this->app['twig']->render('Landing/view/index.twig', [
        ]);
    }
}