<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.9.1
 * Time: 16:45
 */

namespace SRC\Sources\Services;

use SRC\Sources\Repository\SourcesRepository;

class SourcesService
{

    private $sourcesRepository;

    public function __construct(SourcesRepository $sourcesRepository)
    {
        $this->sourcesRepository = $sourcesRepository;
    }

    public function getAll()
    {
        return $this->sourcesRepository->getAll();
    }

    public function getById($id)
    {
        return $this->sourcesRepository->getById($id);
    }

    public function saveSource(Array $params)
    {
        $id = $params['id'];

        $params['clientId'] = '0';
        if ($id && $this->sourcesRepository->getById($id)) {
            $this->sourcesRepository->update($params);
        } else {
            $this->sourcesRepository->save($params);
        }
    }

    public function removeById($id)
    {
        if (!$this->sourcesRepository->getById($id)) {
            throw new \Exception('not found');
        }
        return  $this->sourcesRepository->removeById($id);
    }

}