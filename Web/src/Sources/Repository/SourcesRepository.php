<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.9.1
 * Time: 15:30
 */

namespace SRC\Sources\Repository;

use Doctrine\DBAL\Connection;
use PDO;

class SourcesRepository
{
    private $connection;
    const TABLE = 'sources';

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAll()
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from(self::TABLE, 'src');
        $results = $queryBuilder->execute()->fetchAll();
        return $results;
    }

    public function getById($id)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from(self::TABLE, 'src')
            ->where("src.id = :id")
            ->setParameter(':id', $id);
        $results = $queryBuilder->execute()->fetch();
        return $results;
    }

    public function save(Array $params)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        if (!$params['srcWidth']) {
            $params['srcWidth'] = null;
        }
        if (!$params['srcHeight']) {
            $params['srcHeight'] = null;
        }
        $saved = $queryBuilder
            ->insert(self::TABLE)
            ->values([
                'name' => ':name',
                'src' => ':src',
                'width' => ':width',
                'height' => ':height',
                'client_id' => ':client_id',
                'description' => ':description',
            ])
            ->setParameter(':name', $params['srcName'])
            ->setParameter(':src', $params['srcUrl'])
            ->setParameter(':width', $params['srcWidth'], PDO::PARAM_INT)
            ->setParameter(':height', $params['srcHeight'], PDO::PARAM_INT)
            ->setParameter(':client_id', $params['clientId'])
            ->setParameter(':description', $params['srcDescription'])
            ->execute();
        if ($saved) {
            $id = $this->connection->lastInsertId();
            return $id;
        } else {
            throw new \Exception('Not saved');
        }
    }

    public function update(Array $params)
    {
        if (!$params['srcWidth']) {
            $params['srcWidth'] = null;
        }
        if (!$params['srcHeight']) {
            $params['srcHeight'] = null;
        }
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->update(self::TABLE, 'src')
            ->set('name', ':name')
            ->set('src', ':src')
            ->set('width', ':width')
            ->set('height', ':height')
            ->set('client_id', ':client_id')
            ->set('description', ':description')
            ->where('src.id = :id')
            ->setParameter(':id', $params['id'])
            ->setParameter(':name', $params['srcName'])
            ->setParameter(':src', $params['srcUrl'])
            ->setParameter(':width', $params['srcWidth'], PDO::PARAM_INT)
            ->setParameter(':height', $params['srcHeight'], PDO::PARAM_INT)
            ->setParameter(':client_id', $params['clientId'])
            ->setParameter(':description', $params['srcDescription'])
            ->execute();
    }

    public function removeById($id)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder->delete(self::TABLE)
            ->where('id = :id')
            ->setParameter(':id', $id);
        return $queryBuilder->execute();
    }

}