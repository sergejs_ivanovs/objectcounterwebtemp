<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.8.1
 * Time: 13:26
 */

namespace SRC\Sources\Controller;

use Silex\Application;
use SRC\Sources\Services\SourcesService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SourcesController
{
    private $app;
    private $sourcesService;

    public function __construct(Application $app, SourcesService $sourcesService)
    {
        $this->app = $app;
        $this->sourcesService = $sourcesService;
    }

    public function indexAction()
    {
        $sources = $this->sourcesService->getAll();
        return $this->app['twig']->render('Sources/view/index.twig', [
            'sources' => $sources
        ]);
    }

    public function getAction($id)
    {
        if (!$id) {
            $response = ['status' => ERROR_CODE, 'message' => 'Invalid arguments'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }
        try {
            $data = $this->sourcesService->getById($id);
        } catch (\Exception $e) {
            $response = ['status' => ERROR_CODE, 'message' => $e->getMessage()];
            return $this->app->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $response = ['status' => SUCCESS_CODE, 'message' => 'Data loded', 'data' => $data];
        return $this->app->json($response, Response::HTTP_OK);
    }

    public function saveAction(Request $request)
    {
        $params = [
            'id' => $request->get('id'),
            'srcName' => $request->get('srcName'),
            'srcUrl' => $request->get('srcUrl'),
            'srcWidth' => $request->get('srcWidth'),
            'srcHeight' => $request->get('srcHeight'),
            'srcDescription' => $request->get('srcDescription'),
        ];

        $error = [];
        if (!$params['srcName']) {
            $error[] = [
                'field' => 'srcName',
                'message' => 'Source name required'
            ];
        }
        if (!$params['srcUrl']) {
            $error[] = [
                'field' => 'srcUrl',
                'message' => 'Source url required'
            ];
        }
        if (!empty($error)) {
            $response = ['status' => ERROR_CODE, 'message' => $error[0]['message'], 'error' => $error];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            $this->sourcesService->saveSource($params);
        } catch (\Exception $e) {
            $response = ['status' => ERROR_CODE, 'message' => 'Error on save service'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }

        $response = ['status' => SUCCESS_CODE, 'message' => 'Saved'];
        return $this->app->json($response, Response::HTTP_OK);
    }

    public function deleteAction($id)
    {
        if (!$id) {
            $response = ['status' => ERROR_CODE, 'message' => 'Invalid arguments'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }
        try {
            $this->sourcesService->removeById($id);
        } catch (\Exception $e) {
            $response = ['status' => ERROR_CODE, 'message' => $e->getMessage()];
            return $this->app->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $response = ['status' => SUCCESS_CODE, 'message' => 'removed'];
        return $this->app->json($response, Response::HTTP_OK);
    }
}