$(function(){
    $('#sourcesTable').DataTable();
    var resetSrcModal = function() {
        $('#source-id').val('');
        $('#source-name').val('');
        $('#source-url').val('');
        $('#source-width').val('');
        $('#source-height').val('');
        $('#source-description').val('');
    };

    $('#add-src-btn').click(function() {
        resetSrcModal();
        var modal = $('.edit-src-modal');
        $('#modal-edit-header').addClass('hidden');
        $('#modal-create-header').removeClass('hidden');
        $('.modal-overlay').addClass('hidden');
    });
    $('.sources-remove-btn').click(function() {
        if(!confirm('Are you sure?!')) {
            return false;
        }
        var id = $(this).data('id');
        $.ajax({
            url: '/api/v1/sources/delete/'+id,
            method: "DELETE",
            success: function(result) {
                $('.modal-overlay').addClass('hidden');
                console.log(result);
                if(result.status == 'success') {
                    showSuccess(result.message);
                    location.reload();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                checkXhr(jqXHR);
                $('.modal-overlay').addClass('hidden');
                showError(jqXHR.responseJSON.message);
                console.log(jqXHR.responseJSON);
            }
        });
    });
    $('.sources-edit-btn').click(function(){
        $('#modal-edit-header').removeClass('hidden');
        $('#modal-create-header').addClass('hidden');
        $('.modal-overlay').removeClass('hidden');
        resetSrcModal();
        var id = $(this).data('id');
        $.ajax({
            url: '/api/v1/sources/'+id,
            method: "GET",
            success: function(result) {
                $('.modal-overlay').addClass('hidden');
                console.log(result);
                if(result.status == 'success') {
                    showSuccess(result.message);
                    var id = $('#source-id').val(result.data.id);
                    $('#source-name').val(result.data.name);
                    $('#source-url').val(result.data.src);
                    $('#source-width').val(result.data.width);
                    $('#source-height').val(result.data.height);
                    $('#source-description').val(result.data.description);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                checkXhr(jqXHR);
                $('.modal-overlay').addClass('hidden');
                showError(jqXHR.responseJSON.message);
                console.log(jqXHR.responseJSON);
            }
        });
    });
    $('#modal-source-save-btn').click(function() {
        var id = $('#source-id').val();
        var srcName = $('#source-name').val();
        var srcUrl = $('#source-url').val();
        var srcWidth = $('#source-width').val();
        var srcHeight = $('#source-height').val();
        var srcDescription = $('#source-description').val();

        $('.modal-overlay').removeClass('hidden');
        $.ajax({
            url: '/api/v1/sources/save',
            method: "POST",
            dataType: "JSON",
            data: {
                id: id,
                srcName: srcName,
                srcUrl: srcUrl,
                srcWidth: srcWidth,
                srcHeight: srcHeight,
                srcDescription: srcDescription
            },
            success: function(result) {
                $('.modal-overlay').addClass('hidden');
                if(result.status == 'success') {
                    showSuccess(result.message);
                    location.reload();
                } else {

                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                checkXhr(jqXHR);
                $('.modal-overlay').addClass('hidden');
                showError(jqXHR.responseJSON.message);
                console.log(jqXHR.responseJSON);
            }
        });
    });
});