$(function() {
    $('#admin-alert-close').click(function() {
        hideAlert();
    });
});

var errorpromise = null;

function showError(text) {
    console.log('showError');
    $('.admin-alert')
        .addClass('alert-danger')
        .removeClass('hidden')
        .removeClass('alert-success')
    ;
    $('#general-alert-content').html('<strong>Error! </strong>' + text);
    clearTimeout(errorpromise);
    // setTimeout(function() {
    //     $('.admin-alert').addClass('hidden');
//         $('#general-alert-content').empty();
    // }, 3000);
}
function showSuccess(text) {
    console.log('showSuccess');
    $('.admin-alert')
        .addClass('alert-success')
        .removeClass('hidden')
        .removeClass('alert-danger')
    ;
    $('#general-alert-content').html('<strong>Success! </strong>' + text);
    errorpromise = setTimeout(function() {
        $('.admin-alert').addClass('hidden');
        $('#general-alert-content').empty();
    }, 3000);
}
function hideAlert() {
    $('.admin-alert').addClass('hidden');
    $('#general-alert-content').empty();
}

function checkXhr(jqXHR) {
    var data = jqXHR.responseJSON;
    if(!data) {
        location.reload();
        return false;
    }
}
