$.extend({
    getUrlVars: function(){
        var vars = {}, hash;
        var hashes = window.location.search.replace('?', '').split('&');
        if(hashes[0].length > 0) {
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                // vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
        }
        return vars;
    },
    getUrlVar: function(name){
        return $.getUrlVars()[name];
    },
    getUrlhas: function(name) {
        var param = $.getUrlVar(name);
        if(!param || typeof param == "undefined") {
            return false;
        } else {
            return true;
        }
    }
});