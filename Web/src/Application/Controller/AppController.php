<?php

/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 16.9.11
 * Time: 14:25
 */
namespace SRC\Application\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AppController
{
    private $config;
    private $app;

    public function __construct(Application $app, $config)
    {
        $this->config = $config;
        $this->app = $app;
    }

    public function before(Request $request)
    {
        $this->app['session']->migrate();
        $this->app['translator']->setLocale($this->app['session']->get('_locale'));
        if (strpos($request->headers->get('Content-Type'), 'application/json') === 0) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
    }

    public function after(Request $request, Response $response)
    {
        $this->setLastPath($request);
        $response->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate, private');
    }

    public function setLastPath(Request $request)
    {
        $route = $request->get('_route');
        if (!$route || $route == 'changeLocale' || $route == '_wdt') {
            return;
        }
        $attributes = $request->attributes->get('_route_params');
        $query = $request->query->all();
        $attributes = array_merge($query, $attributes);
        try {
            $uri = $this->app['url_generator']->generate($route, $attributes);
        } catch (\Exception $e) {
            return;
        }
        if (strpos($uri, '/api/v') === false) {
            $this->app['session']->set('last_route', $route);
            $this->app['session']->set('last_attributes', $attributes);
            $this->app['session']->set('last_query', $query);
        }
    }

    public function changeLocale(Request $request, $locale)
    {
        if (in_array($locale, $this->config['system']['allowed_locales'])) {
            if ($this->app['session']->get('_locale') != $locale) {
                $this->app['session']->set('_locale', $locale);
            }
        }

        try{
            $route = $this->app['session']->get('last_route');
            $attributes = $this->app['session']->get('last_attributes');
            $query = $this->app['session']->get('last_query');
            if(is_array($query) && is_array($attributes)) {
                $attributes = array_merge($query, $attributes);
            }
            $uri = $this->app['url_generator']->generate($route, $attributes);
            return $this->app->redirect($uri);
        } catch (\Exception $e) {
            $uri = $this->app['url_generator']->generate('landing');
            return $this->app->redirect($uri);
        }
    }
}