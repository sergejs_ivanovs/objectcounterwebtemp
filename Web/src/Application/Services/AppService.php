<?php
namespace SRC\Application\Services;

use SRC\Application\Services\Interfaces\AppServiceHandler;

class AppService implements AppServiceHandler
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

}