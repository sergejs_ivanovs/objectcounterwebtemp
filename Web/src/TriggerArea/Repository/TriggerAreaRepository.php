<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.12.1
 * Time: 15:10
 */

namespace SRC\TriggerArea\Repository;

use Doctrine\DBAL\Connection;
use PDO;

class TriggerAreaRepository
{
    private $connection;
    const TABLE = 'trigger_area';

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getById($id)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from(self::TABLE, 'area')
            ->where("area.id = :id")
            ->setParameter(':id', $id);
        $results = $queryBuilder->execute()->fetch();
        return $results;
    }

    public function getTriggersByWorkingId($workingAreaId)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from(self::TABLE, 'area')
            ->where("area.working_area_id = :id")
            ->setParameter(':id', $workingAreaId);
        $results = $queryBuilder->execute()->fetchAll();
        return $results;
    }

    public function save(Array $params)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $saved = $queryBuilder
            ->insert(self::TABLE)
            ->values([
                'id' => ':id',
                'name' => ':name',
                'polygon_type' => ':type',
                'working_area_id' => ':working_area_id',
                'polygon' => ':polygon',
                'method_id' => ':method_id',
                'algorithm' => ':algorithm',
                'description' => ':description',
            ])
            ->setParameter(':id', $params['id'], PDO::PARAM_INT)
            ->setParameter(':name', $params['name'])
            ->setParameter(':type', $params['type'])
            ->setParameter(':working_area_id', $params['workingAreaId'])
            ->setParameter(':polygon', $params['polygon'])
            ->setParameter(':method_id', $params['methodId'])
            ->setParameter(':algorithm', $params['algorithm'])
            ->setParameter(':description', $params['description'])
            ->execute();
        if ($saved) {
            $id = $this->connection->lastInsertId();
            return $id;
        } else {
            throw new \Exception('Not saved');
        }
    }

    public function update(Array $params)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->update(self::TABLE, 'area')
            ->set('name', ':name')
            ->set('polygon_type', ':polygon_type')
            ->set('polygon', ':polygon')
            ->set('method_id', ':method_id')
            ->set('algorithm', ':algorithm')
            ->set('description', ':description')
            ->where('area.id = :id')
            ->setParameter(':id', $params['id'])
            ->setParameter(':name', $params['name'])
            ->setParameter(':polygon_type', $params['type'])
            ->setParameter(':polygon', $params['polygon'])
            ->setParameter(':method_id', $params['methodId'])
            ->setParameter(':algorithm', $params['algorithm'])
            ->setParameter(':description', $params['description'])
            ->execute();
    }

    public function removeById($id)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder->delete(self::TABLE)
            ->where('id = :id')
            ->setParameter(':id', $id);
        return $queryBuilder->execute();
    }
}