<?php

namespace SRC\TriggerArea\Services;

use SRC\TriggerArea\Repository\TriggerAreaRepository;

class TriggerAreaService
{
    private $triggerAreaRepository;

    public function __construct(TriggerAreaRepository $triggerAreaRepository)
    {
        $this->triggerAreaRepository = $triggerAreaRepository;
    }

    public function saveAreaPosition(Array $params)
    {
        $id = $params['id'];
        $params['polygon'] = json_encode($params['polygon']);
        $params['clientId'] = '0';
        $area = $this->triggerAreaRepository->getById($id);
        if ($id && $area) {
            $params['methodId'] = $area['method_id'];
            $params['algorithm'] = $area['algorithm'];
            $params['description'] = $area['description'];
            $this->triggerAreaRepository->update($params);
        } else {
            $params['methodId'] = null;
            $params['algorithm'] = null;
            $params['description'] = null;
            $id = $this->triggerAreaRepository->save($params);
        }

        return $id;
    }

    public function removeById($id)
    {
        if (!$this->triggerAreaRepository->getById($id)) {
            throw new \Exception('not found');
        }
        return  $this->triggerAreaRepository->removeById($id);
    }

    public function getTriggerAreas($workingAreaId)
    {
        $areas = $this->triggerAreaRepository->getTriggersByWorkingId($workingAreaId);
        foreach ($areas as $key => $area) {
            $polygonPoints = json_decode($area['polygon']);
            foreach ($polygonPoints as $pkey => $point) {
                $polygonPoints[$pkey][0] = (float)$point[0];
                $polygonPoints[$pkey][1] = (float)$point[1];
            }
            $areas[$key]['polygon'] = $polygonPoints;
        }

        return $areas;
    }
}