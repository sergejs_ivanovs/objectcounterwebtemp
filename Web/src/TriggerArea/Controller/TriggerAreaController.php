<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.11.1
 * Time: 16:41
 */

namespace SRC\TriggerArea\Controller;

use Silex\Application;
use SRC\TriggerArea\Services\TriggerAreaService;
use SRC\WorkingAreas\Services\WorkingAreaService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TriggerAreaController
{
    private $app;
    private $triggerAreaService;
    private $workingAreaService;

    public function __construct(Application $app,
                                TriggerAreaService $triggerAreaService,
                                WorkingAreaService $workingAreaService
    )
    {
        $this->app = $app;
        $this->triggerAreaService = $triggerAreaService;
        $this->workingAreaService = $workingAreaService;
    }

    public function savePositionAction(Request $request)
    {
        $params = [
            'id' => $request->get('id'),
            'name' => $request->get('name'),
            'type' => $request->get('type'),
            'workingAreaId' => $request->get('workingAreaId'),
            'polygon' => $request->get('polygon'),
        ];
        $error = [];
        if (!$params['name']) {
            $error[] = [
                'field' => 'name',
                'message' => 'Trigger area name required'
            ];
        }
        if (!$params['workingAreaId'] || !$this->workingAreaService->getById($params['workingAreaId'])) {
            $error[] = [
                'field' => 'workingAreaId',
                'message' => 'Working Area Id required'
            ];
        }
        if (!empty($error)) {
            $response = ['status' => ERROR_CODE, 'message' => $error[0]['message'], 'error' => $error];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }

        try {
            $areaId = $this->triggerAreaService->saveAreaPosition($params);
        } catch (\Exception $e) {
            $response = ['status' => ERROR_CODE, 'message' => 'Error on save service'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }

        $response = ['status' => SUCCESS_CODE, 'message' => 'Saved', 'data' => ['id' => $areaId]];
        return $this->app->json($response, Response::HTTP_OK);
    }

    public function deleteAction($id)
    {
        if (!$id) {
            $response = ['status' => ERROR_CODE, 'message' => 'Invalid arguments'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }
        try {
            $this->triggerAreaService->removeById($id);
        } catch (\Exception $e) {
            $response = ['status' => ERROR_CODE, 'message' => $e->getMessage()];
            return $this->app->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $response = ['status' => SUCCESS_CODE, 'message' => 'removed'];
        return $this->app->json($response, Response::HTTP_OK);
    }

    public function getAction($workingAreaId)
    {
        try {
            $data = $this->triggerAreaService->getTriggerAreas($workingAreaId);
        } catch (\Exception $e) {
            $response = ['status' => ERROR_CODE, 'message' => 'Error on fetching data'];
            return $this->app->json($response, Response::HTTP_BAD_REQUEST);
        }
        $response = ['status' => SUCCESS_CODE, 'message' => 'Triggers Fetched', 'data' => $data];
        return $this->app->json($response, Response::HTTP_OK);
    }
}