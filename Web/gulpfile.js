var gulp = require('gulp'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');

gulp.task('app', function() {
    return gulp.src([
            'src/**/assets/js/*.js'
        ])
        .pipe(concat('main.app.min.js'))
        // .pipe(gulp.dest('web/js'))
        // .pipe(uglify())
        .pipe(gulp.dest('web/js'));
});

gulp.task('styles', function() {
    return gulp.src([
        'src/**/assets/css/*.css'
    ])
        .pipe(concat('main.app.min.css'))
        .pipe(gulp.dest('web/css'));
});

gulp.task('watch', ['styles', 'app'], function() {
    gulp.watch('src/**/assets/css/**/*.css', ['styles']);
    gulp.watch('src/**/assets/js/**/*.js', ['app']);
});

gulp.task('default', [
    'app',
    'styles'
]);
