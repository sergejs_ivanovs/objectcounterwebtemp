<?php
/********************************************************
 * Services
 ********************************************************/
use SRC\Application\Services\AppService;
use SRC\Sources\Repository\SourcesRepository;
use SRC\Sources\Services\SourcesService;
use SRC\TriggerArea\Repository\TriggerAreaRepository;
use SRC\TriggerArea\Services\TriggerAreaService;
use SRC\WorkingAreas\Repository\WorkingAreaRepository;
use SRC\WorkingAreas\Services\WorkingAreaService;

/**
 * Repositories
 */
$app['repositories.SourcesRepository'] = function () use($app) {
    return new SourcesRepository($app['db']);
};
$app['repositories.WorkingAreaRepository'] = function () use($app) {
    return new WorkingAreaRepository($app['db']);
};
$app['repositories.TriggerAreaRepository'] = function () use($app) {
    return new TriggerAreaRepository($app['db']);
};

/**
 * Services
 */
$app['services.AppService'] = function () use($app, $config) {
    return new AppService($config);
};
$app['services.SourcesService'] = function () use($app) {
    return new SourcesService($app['repositories.SourcesRepository']);
};
$app['services.WorkingAreaService'] = function () use($app) {
    return new WorkingAreaService($app['repositories.WorkingAreaRepository']);
};
$app['services.TriggerAreaService'] = function () use($app) {
    return new TriggerAreaService($app['repositories.TriggerAreaRepository']);
};

