<?php
require __DIR__ . '/../vendor/autoload.php';

use Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand;
use Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand;
use Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand;
use Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand;
use Symfony\Component\Console\Application;

$console = new Application();
//Migrations commands
$console->add(new ExecuteCommand());
$console->add(new GenerateCommand());
$console->add(new MigrateCommand());
$console->add(new StatusCommand());

$console->run();
