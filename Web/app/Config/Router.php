<?php

use SRC\Application\Controller\AppController;
use SRC\Landing\Controller\LandingController;
use SRC\Maintenance\Controller\MaintenanceController;
use SRC\Sources\Controller\SourcesController;
use SRC\TriggerArea\Controller\TriggerAreaController;
use SRC\WorkingAreas\Controller\WorkingAreasController;

/********************************************************
 * Routs/Controller
 ********************************************************/

//A before application middleware allows you to tweak the Request before the controller is executed:
$app['controller.app'] = function () use ($config, $app) {
    return new AppController($app, $config, $app['services.AppService']);
};
    $app->before('controller.app:before');
    //An after application middleware allows you to tweak the Response before it is sent to the client:
    $app->after('controller.app:after');
/**********
 * AppController
 *********/
    $app->get('/language/{locale}', 'controller.app:changeLocale')->bind('changeLocale');

/**********
 * Landing
 *********/
$app['controller.landing'] = function () use ($app) {
    return new LandingController($app);
};
    $app->get('/', 'controller.landing:indexAction')->bind('landing');

/**********
 * WorkingAreas
 *********/
$app['controller.working_areas'] = function () use ($app) {
    return new WorkingAreasController($app, $app['services.WorkingAreaService'], $app['services.SourcesService']);
};
    $app->get('/working_area', 'controller.working_areas:indexAction')->bind('workingAreas');
    $app->get('/working_area/{id}', 'controller.working_areas:openAreaAction')->bind('workingAreaOpen');
    $app->post('/api/v1/working_area/save', 'controller.working_areas:saveAction')->bind('workingAreaSave');
    $app->get('/api/v1/working_area/get/{id}', 'controller.working_areas:getAction')->bind('workingAreaget');
    $app->delete('/api/v1/working_area/delete/{id}', 'controller.working_areas:deleteAction')->bind('workingAreaRemove');

/**********
 * TriggersAreas
 *********/
$app['controller.trigger_areas'] = function () use ($app) {
    return new TriggerAreaController($app, $app['services.TriggerAreaService'], $app['services.WorkingAreaService']);
};
    $app->post('/api/v1/trigger_areas/save', 'controller.trigger_areas:savePositionAction')->bind('triggerAreaSave');
    $app->get('/api/v1/trigger_areas/get/{workingAreaId}', 'controller.trigger_areas:getAction')->bind('triggerAreaGet');
    $app->delete('/api/v1/sources/delete/{id}', 'controller.trigger_areas:deleteAction')->bind('triggerAreaDelete');

/**********
 * Sources
 *********/
$app['controller.sources'] = function () use ($app) {
    return new SourcesController($app, $app['services.SourcesService']);
};
    $app->get('/sources', 'controller.sources:indexAction')->bind('sources');
    $app->post('/api/v1/sources/save', 'controller.sources:saveAction')->bind('sourcesSave');
    $app->delete('/api/v1/sources/delete/{id}', 'controller.sources:deleteAction')->bind('sourceDelete');
    $app->get('/api/v1/sources/{id}', 'controller.sources:getAction')->bind('sourceGet');


/**********
 * Maintenance
 *********/
    $app['controller.maintenance'] = function () use ($app) {
        return new MaintenanceController($app);
    };
    if($config['system']['maintenance']) {
        $app->get('/maintenance', 'controller.maintenance:indexAction')->bind('maintenance');
    } else {
        $app->get('/maintenance', function() use($app) {
            $uri = $app['url_generator']->generate('landing');
            return $app->redirect($uri);
        });
    }
