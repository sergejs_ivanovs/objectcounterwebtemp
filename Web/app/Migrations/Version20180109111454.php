<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180109111454 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            CREATE TABLE users (
                id INT(11) NOT NULL AUTO_INCREMENT,
                username VARCHAR(32) NOT NULL,
                password VARCHAR(255) NOT NULL,
                roles VARCHAR(255) NOT NULL,
                created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (id),
                UNIQUE INDEX username (username)
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
            ;
        ');
        $this->addSql('
            CREATE TABLE sources (
                id INT NOT NULL AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL,
                src VARCHAR(1000) NOT NULL,
                width INT NULL,
                height INT NULL,
                client_id INT NULL,
                description VARCHAR(255) NULL,
                created DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
                updated DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (id)
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
            ;
        ');
        $this->addSql("
            CREATE TABLE working_area (
                id INT NOT NULL AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL,
                source_id INT NOT NULL,
                client_id INT NULL,
                polygon JSON NULL,
                description VARCHAR(255) NULL,
                created DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
                updated DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (id),
                INDEX FK__sources (source_id),
                CONSTRAINT FK__sources FOREIGN KEY (source_id) REFERENCES sources (id) ON UPDATE CASCADE ON DELETE CASCADE
            )
            COLLATE='utf8_unicode_ci'
            ENGINE=InnoDB
            ;
        ");
        $this->addSql('
            CREATE TABLE trigger_area (
                id BIGINT NOT NULL AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL,
                polygon_type VARCHAR(50) NOT NULL DEFAULT \'polygon\',
                working_area_id INT NOT NULL,
                polygon JSON NULL,
                method_id INT NULL,
                algorithm VARCHAR(50) NULL,
                description VARCHAR(255) NULL,
                created DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
                updated DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (id),
                CONSTRAINT FK__working_area FOREIGN KEY (working_area_id) REFERENCES working_area (id) ON UPDATE CASCADE ON DELETE CASCADE
            )
            COLLATE=\'utf8_unicode_ci\'
            ENGINE=InnoDB
            ;
        ');
        $this->addSql('
            CREATE TABLE tasks (
                id INT NOT NULL AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL,
                description VARCHAR(255) NULL,
                working_area_id INT NOT NULL,
                worker_id INT NOT NULL,
                start_time DATETIME NOT NULL,
                end_time DATETIME NOT NULL,
                product_id INT NOT NULL,
                created DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
                updated DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (id)
            )
            COLLATE=\'utf8_unicode_ci\'
            ;
        ');
        $this->addSql('
            CREATE TABLE workers (
                id INT NOT NULL AUTO_INCREMENT,
                first_name VARCHAR(50) NOT NULL,
                last_name VARCHAR(50) NOT NULL,
                person_id VARCHAR(50) NOT NULL,
                position VARCHAR(50) NOT NULL,
                created DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
                updated DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (id)
            )
            COLLATE=\'utf8_unicode_ci\'
            ;
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            SET FOREIGN_KEY_CHECKS=0;
            DROP TABLE users;
            DROP TABLE sources;
            DROP TABLE working_area;
            DROP TABLE trigger_area;
            DROP TABLE tasks;
            DROP TABLE workers;
            SET FOREIGN_KEY_CHECKS=1;
        ');
    }
}
