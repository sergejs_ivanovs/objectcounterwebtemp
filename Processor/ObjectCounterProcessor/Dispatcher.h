#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <QObject>
#include "Repository.h"
#include "Processor.h"
#include "Scheduler.h"
#include "VideoInput.h"

class Dispatcher : public QObject
{
    Q_OBJECT

public:
    explicit Dispatcher(QObject *parent = 0);
    ~Dispatcher();

signals:

public slots:
    void startProcessing(QString, int);

private:
    QString videoFileDir;
    Repository *repository;
    Processor *processor;
    Scheduler *scheduler;
    VideoInput *videoInput;
};

#endif // DISPATCHER_H
