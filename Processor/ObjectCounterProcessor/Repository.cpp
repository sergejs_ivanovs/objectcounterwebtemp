#include "Repository.h"

Repository::Repository(QObject *parent) : QObject(parent)
{
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("172.19.0.1");
    db.setDatabaseName("drw");
    db.setUserName("test");
    db.setPassword("test");
    bool ok = db.open();
    qDebug() << "Connection " << ok;
}
Repository::~Repository()
{
    db.close();
}

bool Repository::beginTransaction()
{
    if(!db.transaction()){
        qWarning() << "Failed to begin transaction!";
        return false;
    }
    return true;
}

bool Repository::commitTransaction()
{
    if(!db.commit()){
        qWarning() << "Failed to commit transaction!";
        return false;
    }
    return true;
}

bool Repository::rollbackTransaction()
{
    return db.rollback();
}

Repository::Source Repository::getSourceById(int id)
{
    if(!db.isOpen()){
        qDebug() << " No connection to db";
        throw std::string("Could not connect to database.");
    }
    QSqlQuery query(db);
    query.prepare(
        "SELECT * FROM drw.sources as src "
        "WHERE src.id = :id"
    );
    query.bindValue(":id", id);
    if(query.exec()) {
        if (query.next()) {
            Repository::Source source;
            source.id = query.value(0).toLongLong();
            source.name = query.value(1).toString();
            source.src = query.value(2).toString();
            source.width = query.value(3).toInt();
            source.height = query.value(4).toInt();
            source.clientId = query.value(5).toInt();
            source.description = query.value(6).toString();
            source.created = query.value(7).toDateTime();
            source.updated = query.value(8).toDateTime();
            return source;
        }
    } else {
        qDebug() << "query exec failed ";
        qDebug() << db.lastError();
        qDebug() << query.lastError();
        throw std::string("Could not execute query");
    }
}

Repository::WorkingArea Repository::getWorkingAreaById(int id)
{
    if(!db.isOpen()){
        qDebug() << " No connection to db";
        throw std::string("Could not connect to database.");
    }
    QSqlQuery query(db);
    query.prepare(
        "SELECT * FROM drw.working_area as area "
        "WHERE area.id = :id"
    );
    query.bindValue(":id", id);
    if(query.exec()) {
        if (query.next()) {
            Repository::WorkingArea area;
            area.id = query.value(0).toLongLong();
            area.name = query.value(1).toString();
            area.sourceId = query.value(2).toInt();
            area.clientId = query.value(3).toInt();
            area.polygon = QJsonDocument::fromJson(query.value(4).toString().toUtf8()).array();
            area.description = query.value(5).toString();
            area.created = query.value(6).toDateTime();
            area.updated = query.value(7).toDateTime();
            return area;
        }
    } else {
        qDebug() << "query exec failed ";
        qDebug() << db.lastError();
        qDebug() << query.lastError();
        throw std::string("Could not execute query");
    }
}

QVector<Repository::TriggerArea> Repository::getTriggrsByWorkingAreaId(int id)
{
    if(!db.isOpen()){
        qDebug() << " No connection to db";
        throw std::string("Could not connect to database.");
    }
    QSqlQuery query(db);
    query.prepare(
        "SELECT * FROM drw.trigger_area as area "
        "WHERE area.working_area_id = :id"
    );
    query.bindValue(":id", id);
    if(query.exec()) {
        qDebug() << "query exec success ";
        QVector<Repository::TriggerArea> triggerAreas;
        while (query.next()) {
            Repository::TriggerArea triggerArea;
            triggerArea.id = query.value(0).toLongLong();
            triggerArea.name = query.value(1).toString();
            triggerArea.polygonType = query.value(2).toString();
            triggerArea.workingAreaId = query.value(3).toInt();
            triggerArea.polygon = QJsonDocument::fromJson(query.value(4).toString().toUtf8()).array();
            triggerArea.methodId = query.value(5).toInt();
            triggerArea.algorithm = query.value(6).toString();
            triggerArea.description = query.value(7).toString();
            triggerArea.created = query.value(8).toDateTime();
            triggerArea.updated = query.value(9).toDateTime();
            triggerAreas.push_back(triggerArea);
        }
        return triggerAreas;
    } else {
        qDebug() << "query exec failed ";
        qDebug() << db.lastError();
        qDebug() << query.lastError();
        throw std::string("Could not execute query");
    }
}


