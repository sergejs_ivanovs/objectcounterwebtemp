#include "VideoInput.h"

VideoInput::VideoInput(QObject *parent) : QObject(parent)
{
    path = "";
    video = new cv::VideoCapture(path.toStdString());
    fps = 0;
}

VideoInput::~VideoInput()
{
    video->release();
    delete video;
}

void VideoInput::loadVideo(QString path)
{
    this->path = path;
    video->release();
    video->open(this->path.toStdString());
    if(!video->isOpened()) {
        qDebug() << "Could not read video file" << endl;
        throw std::string("Could not read video file");
    }
    fps = video->get(CV_CAP_PROP_FPS);
}

bool VideoInput::getFrame(cv::Mat &frame)
{
    return video->read(frame);
}

double VideoInput::getFps()
{
    return fps;
}
