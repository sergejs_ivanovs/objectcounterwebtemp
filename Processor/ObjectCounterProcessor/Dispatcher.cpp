#include "Dispatcher.h"
#include <QDebug>

#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>

Dispatcher::Dispatcher(QObject *parent) : QObject(parent)
{
    videoFileDir = "";
    repository = new Repository();
    processor = new Processor();
    scheduler = new Scheduler();
    videoInput = new VideoInput();
}

Dispatcher::~Dispatcher()
{
    delete repository;
    delete processor;
    delete scheduler;
    delete videoInput;
}

void Dispatcher::startProcessing(QString path, int workingAreaId)
{
    qDebug() << "startProcessing";

    Repository::WorkingArea workingArea = repository->getWorkingAreaById(workingAreaId);
    QVector<Repository::TriggerArea> triggerAreas = repository->getTriggrsByWorkingAreaId(workingAreaId);
    Repository::Source source = repository->getSourceById(workingArea.sourceId);

    videoFileDir = path;
    videoInput->loadVideo(source.src);

    double fps = videoInput->getFps();
    double time = 0;
    qDebug() << "video fps: " << fps;
    cv::Mat frame;
    bool firstFrame = true;
    while(videoInput->getFrame(frame)) {
        if(firstFrame) {
            firstFrame = false;
        } else {
            time += 1/fps;
        }
        processor->frameProcessing(frame, time);
    }
}
