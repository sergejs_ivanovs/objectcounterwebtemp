#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QObject>
#include <QDebug>
#include <opencv2/opencv.hpp>

class Processor : public QObject
{
    Q_OBJECT
public:
    explicit Processor(QObject *parent = 0);
    ~Processor();

signals:

public slots:
    void frameProcessing(cv::Mat frame, double time);
};

#endif // PROCESSOR_H
