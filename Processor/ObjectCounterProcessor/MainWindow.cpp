#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QDir>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    dispatcher = new Dispatcher();
    fileDir = "";
    ui->filePath->setText(fileDir);

    thread = new QThread();
    //connections
    connect(this, SIGNAL(startButtonPressed(QString, int)), dispatcher, SLOT(startProcessing(QString, int)));
    //start thread
    dispatcher->moveToThread(thread);
    thread->start();

}

MainWindow::~MainWindow()
{
    delete ui;
    delete dispatcher;
//    delete thread;
}

void MainWindow::on_browseButton_clicked()
{
    qDebug() << "browse clicked";
    fileDir = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                 QDir::homePath());
    qDebug() << "file selected " << fileDir;
    if(!fileDir.trimmed().isEmpty()) {
        ui->filePath->setText(fileDir);
    }
}

void MainWindow::on_startButton_clicked()
{
    int id = ui->workingAreaId->value();
    qDebug() << "start clicked";
    if(!fileDir.trimmed().isEmpty()) {
        qDebug() << "file not provided ";
    } else {
        emit startButtonPressed(fileDir, id);
    }
}
