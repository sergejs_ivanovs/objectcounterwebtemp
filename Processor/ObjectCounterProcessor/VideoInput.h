#ifndef VIDEOINPUT_H
#define VIDEOINPUT_H

#include <QObject>
#include <QDebug>
#include <opencv2/opencv.hpp>

class VideoInput : public QObject
{
    Q_OBJECT
public:
    explicit VideoInput(QObject *parent = 0);
    ~VideoInput();

signals:

public slots:
    void loadVideo(QString path);
    bool getFrame(cv::Mat &frame);
    double getFps();

private:
    QString path;
    cv::VideoCapture *video;
    double fps;
};

#endif // VIDEOINPUT_H
