#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "Dispatcher.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

signals:
    void startButtonPressed(QString path, int id);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_browseButton_clicked();

    void on_startButton_clicked();

private:
    Ui::MainWindow *ui;
    QString fileDir;
    Dispatcher *dispatcher;
    QThread *thread;
};

#endif // MAINWINDOW_H
