#ifndef REPOSITORY_H
#define REPOSITORY_H

#include <QObject>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

class Repository : public QObject
{
    Q_OBJECT
public:
    explicit Repository(QObject *parent = 0);
    ~Repository();

    struct Source {
        int id;
        QString name;
        QString src;
        int width;
        int height;
        int clientId;
        QString description;
        QDateTime created;
        QDateTime updated;
    };
    struct Task {
        int id;
        QString name;
        QString description;
        int workingAreaId;
        int workerId;
        QDateTime startTime;
        QDateTime endTime;
        int productId;
        QDateTime created;
        QDateTime updated;
    };
    struct TriggerArea {
        long id;
        QString name;
        QString polygonType;
        int workingAreaId;
        QJsonArray polygon;
        int methodId;
        QString algorithm;
        QString description;
        QDateTime created;
        QDateTime updated;
    };
    struct Worker {
        int id;
        QString firstName;
        QString lastName;
        QString personId;
        QString position;
        QDateTime created;
        QDateTime updated;
    };
    struct WorkingArea {
        int id;
        QString name;
        int sourceId;
        int clientId;
        QJsonArray polygon;
        QString description;
        QDateTime created;
        QDateTime updated;
    };

signals:

public slots:
    Repository::Source getSourceById(int id);
    Repository::WorkingArea getWorkingAreaById(int id);
    QVector<Repository::TriggerArea> getTriggrsByWorkingAreaId(int id);
    bool beginTransaction();
    bool commitTransaction();
    bool rollbackTransaction();
private:
    QSqlDatabase db;
};

#endif // REPOSITORY_H
